const database = require('./data')
const moment = require('moment')
const _ = require('lodash')
const request = require('request-promise')
const orders = database.orders

function createOrder(restaurant_id, items) {
  const order = {
    id: 2,
    u_id: 2,
    r_id: 1,
    items: {
      '1': 2,
      '2': 1
    },
    payment_mode: 'credit_card',
    payment_status: 'pending',
    address: '75 Lakshmi Nagar',
    order_time: '22-02-2019',
    amount: 382.0
  }
  orders.push(order)
  console.log(order)
}

function chargeCard(amount, payment_details, headers) {
  request({
    method: 'POST',
    uri: 'http://localhost:3001/charge-card',
    json: true,
    headers: {uid: headers.uid},
    body: {amount, payment_details}
  }).then(console.log).catch(console.log)
}


module.exports = function(app) {
  app.get('/order/:id', (req, res) => {
    let replied = false
    _.each(orders, order => {
      if(order.id == req.param.id) {
        res.status(200).json({data: {order}, error: null})
        replied = true
      }
    })

    if(!replied) {
      res.status(404).json({data: null, error: 'no such order'})
    }
  })

  app.post('/order', (req, res) => {
    console.log(`ORDERSERVICE PORT 3000 ${moment().format("HH:mm:ss.SSS")} uid:`, req.headers.uid)
    console.log("PORT 3000 BODY:", req.body)
    const {
      restaurant_id,
      items,
      payment_details,
    } = req.body
    createOrder(restaurant_id, items)
    chargeCard(382.0, payment_details, req.headers)
    res.sendStatus(200)
  })

  app.put('/order', (req, res) => {
    const {
      order_id,
      payment_id,
      response,
    } = req.body
    res.sendStatus(200)
    updateOrder(order_id, payment_id, response)
  })
}