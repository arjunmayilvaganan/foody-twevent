const express = require('express')
const _ = require('lodash')
const bodyParser = require('body-parser')


const app1 = express()
const app2 = express()
const app3 = express()

const port1 = 3000
const port2 = 3001
const port3 = 3002

app1.use(bodyParser.json())
app2.use(bodyParser.json())
app3.use(bodyParser.json())

const restaurant = require('./restaurant')(app1)
const search = require('./search')(app1)
const user = require('./user')(app1)
const order = require('./order')(app1)
const payment = require('./payment')(app2)
const orchestrator = require('./orchestrator')(app3)

app1.listen(
  port1, () => console.log(
    `Example app listening on port ${port1}!`
  )
)

app2.listen(
  port2, () => console.log(
    `Example app listening on port ${port2}!`
  )
)

app3.listen(
  port3, () => console.log(
    `Example app listening on port ${port3}!`
  )
)
