const uuid = require('uuid')
const _ = require('lodash')
const request = require('request-promise')
const moment = require('moment')

module.exports = function(app) {
  app.all('*', (req, res) => {
    const headers = _.extend({}, req.headers)
    const body = _.extend({}, req.body)

    console.log("PATH:", req.path)
    console.log(`ORCHESTRATOR PORT 3002 ${moment().format("HH:mm:ss.SSS")} uid:`, headers.uid)
    console.log("BODY:\n", body)
    if(!headers.uid) {
      headers.uid = uuid.v4()
    }
    const path = req.path
    switch(path) {
      case '/order':
        const _req = {
          headers: {uid: headers.uid},
          uri: 'http://localhost:3000/order',
          method: 'POST',
          body: body,
          json: true
        }
        request(_req)
        .then(data => {
          console.log(data)
          res.status(200).json(data)
        })
        .catch(console.log)
        break
      case '/charge-card':
        const __req = {
          headers: {uid: headers.uid},
          uri: 'http://localhost:3001/charge-card',
          method: 'POST',
          body: body,
          json: true
        }
        request(__req)
        .then(data => {
          console.log(data)
          res.status(200).json(data)
        })
        .catch(console.log)
        break
      default:
        break
    }
  })
}