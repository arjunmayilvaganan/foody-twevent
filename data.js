const menu = [
  [
    {
      'name': 'steak',
      'veg': false,
      'price': 350.0
    },
    {
      'name': 'salsa',
      'veg': true,
      'price': 275.0
    },
    {
      'name': 'fried chicken',
      'veg': false,
      'price': 250.0
    }
  ],
  [
    {
      'name': 'pasta',
      'veg': false,
      'price': 150.0
    }, 
    {
      'name': 'red wine',
      'veg': true,
      'price': 450.0
    },
    {
      'name': 'coffee',
      'veg': true,
      'price': 75.0
    }
  ]
]

const restaurants = [
  {
    'id': '1',
    'name': 'jonah\'s bistro',
    'location': 'nungambakkam',
    'cuisine': 'french',
    'rating': 4.9,
    'voters': 8,
    'menu': menu[0]
  },
  {
    'id': '2',
    'name': 'eataly express',
    'location': 'adyar',
    'cuisine': 'italian',
    'rating': 4.3,
    'voters': 10,
    'menu': menu[1]
  }
]

const otherRestaurants = [
  {
    'id': '3',
    'name': 'tastea cafe',
    'location': 'nanganallur',
    'cuisine': 'irish',
    'rating': 4.1,
    'voters': 7,
    'menu': ['coffee', 'tea', 'sandwich']
  },
  {
    'id': '4',
    'name': 'zaitoon restaurant',
    'location': 'velachery',
    'cuisine': 'arabian',
    'rating': 4.7,
    'voters': 12,
    'menu': ['grilled chicken', 'shawarma', 'tandoori chicken']
  },
  {
    'id': '5',
    'name': 'sangeetha hotel',
    'location': 'guindy',
    'cuisine': 'south indian',
    'rating': 4.6,
    'voters': 25,
    'menu': ['idly', 'dosa', 'vada']
  }
]

const users = [
  {
    'id': 1,
    'first_name': 'Arjun',
    'last_name': 'Mayilvaganan',
    'email': 'arjunmayilvaganan@gmail.com',
    'phone': '+919940558107',
    'address': '75 Lakshmi Nagar',
    'password': 'voila',
  }
]

const orders = [
  {
    "id": 1
  }
]

module.exports = {
  restaurants: restaurants,
  users: users,
  orders: orders,
}
