const database = require('./data')
const _ = require('lodash')

const users = database.users
const restaurants = database.restaurants
let userCount = database.users.length

module.exports = function(app) {
  // REGISTER USER
  app.post('/user', (req, res) => {
    const first_name = req.body.first_name
    const last_name = req.body.last_name
    const email = req.body.email
    const phone = req.body.phone
    const address = req.body.address
    const password = req.body.password

    user = {
      id: userCount + 1,
      first_name,
      last_name,
      email,
      phone,
      address,
      password,
    }

    users.push(user)
    userCount++
    console.log(users)
    res.status(200).json(user)
  })

  // AUTH USER
  app.post('/login', (req, res) => {
    email = req.body.email
    password = req.body.password
    let status = 403
    let response = {
      'data': null,
      'error': 'not allowed'
    }
    
    _.each(users, user => {
      if(user.email == email && user.password == password) {
        response.error = null
        response.data = {
          'restaurants': restaurants
        }
      }
    })

    res.status(200).json(response)
  })
}
