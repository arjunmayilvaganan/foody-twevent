const database = require('./data')
const _ = require('lodash')

const restaurants = database.restaurants

module.exports = function(app) {
  // FETCH ALL RESTAURANTS
  app.get('/restaurant', (req, res) => {
    res.send({
      'data': restaurants,
      'error': null
    })
  })

  // GET A RESTAURANT
  app.get('/restaurant/:id', (req, res) => {
    const r_id = req.params.id
    let status = 404
    let response = {
      'data': null,
      'error': 'no such restaurant'
    }
    
    _.each(restaurants, r => {
      if(r.id == r_id) {
        restaurant = _.extend({}, r)
        restaurant.menu = {
          'veg': _.filter(restaurant.menu, item => item.veg),
          'non-veg': _.filter(restaurant.menu, item => !item.veg)
        }
        status = 200
        response.error = null
        response.data = {
          'restaurant': restaurant
        }
      }
    })

    res.status(status).json(response)
  })

  // ADD RATINGS TO RESTAURANTS
  app.post('/restaurant/:id/rating', (req, res) => {
    const r_id = req.body.id
    const rating = req.body.rating
    let response = {
      'data': null,
      'error': 'no such restaurant'
    }
    let status = 404
    
    _.each(restaurants, restaurant => {
      if(restaurant.id == r_id) {
        restaurant.rating = Math.round(
          ((restaurant.rating * restaurant.voters) + rating) / (restaurant.voters + 1) * 10
        ) / 10
        restaurant.voters = restaurant.voters + 1
        response = {
          'data': restaurant,
          'error': null
        }
        status = 200
        return
      }
    })
    
    res.status(status).json(response)
  })
}
