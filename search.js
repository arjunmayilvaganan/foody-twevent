const database = require('./data')
const _ = require('lodash')

const restaurants = database.restaurants

module.exports = function(app) {
  // SEARCH RESTAURANTS BY NAME
  app.get('/search', (req, res) => {
    const r_query = req.query.restaurant
    const result = []
    
    _.each(restaurants, restaurant => {
      (restaurant.name.indexOf(r_query) > -1) && result.push(restaurant)
    })
    
    res.status(200).json({
      'data': result,
      'error': null
    })
  })
}
