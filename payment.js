const moment = require('moment')

module.exports = function(app) {
  app.post('/charge-card', (req, res) => {
    console.log(`PAYMENTSERVICE PORT 3001 ${moment().format("HH:mm:ss.SSS")} uid:`, req.headers.uid)
    console.log(req.body)
    res.sendStatus(200)
  })
}